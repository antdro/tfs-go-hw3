package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"sync"
	"time"
)

const (
	tradeOpening = 7 * time.Hour // from UTC to Moscow time
	tradeClosing = 0 * time.Hour // from UTC to Moscow time
)

type trade struct {
	ticker    string
	cost      float32
	count     int
	startTime time.Time // time.RFC3339
}

type candle struct {
	ticker      string
	startTime   time.Time // time.RFC3339
	costOpening float32
	costMax     float32
	costMin     float32
	costClosing float32
	minutes     int
}

func reader() chan trade {
	out := make(chan trade)
	read := func() {
		var path = flag.String("path", "trades.csv", "path to input file")

		flag.Parse()

		fileInput, err := os.Open(*path)

		if err != nil {
			log.Fatal("cannot read the file: ", *path, ",err: ", err)
		}
		defer fileInput.Close()

		r := csv.NewReader(fileInput)

		for {
			record, err := r.Read()
			if err == io.EOF {
				break
			}

			if err != nil {
				log.Fatal(err)
			}

			c, err := parse(record)
			if err != nil {
				log.Fatal(err)
			}

			if float64(c.startTime.Hour()) > tradeClosing.Hours() && float64(c.startTime.Hour()) < tradeOpening.Hours() {
				continue
			}

			out <- *c
		}
		close(out)
	}

	go read()

	return out
}

func parse(record []string) (*trade, error) {
	var t trade

	tempcost, err := strconv.ParseFloat(record[1], 64)
	if err != nil {
		return nil, fmt.Errorf("can't parse variable 2")
	}

	tempcount, err := strconv.Atoi(record[2])
	if err != nil {
		return nil, fmt.Errorf("can't parse variable 3")
	}

	temptime, err := time.Parse("2006-01-02 15:04:05", record[3])
	if err != nil {
		return nil, fmt.Errorf("can't parse variable 4")
	}

	t.ticker = record[0]
	t.cost = float32(tempcost)
	t.count = tempcount
	t.startTime = temptime

	return &t, nil
}

func compare(cur candle, t trade) candle {
	cost := t.cost

	if cur.costOpening == 0 {
		cur.costOpening = cost
		cur.costMax = cost
		cur.costMin = cost
	} else {
		if cur.costMax < cost {
			cur.costMax = cost
		}
		if cur.costMin > cost {
			cur.costMin = cost
		}
	}

	cur.costClosing = cost

	return cur
}

func candleCompare(cur candle, in candle) candle {
	if cur.costOpening == 0 {
		cur.costOpening = in.costOpening
		cur.costMax = in.costMax
		cur.costMin = in.costMin
	}

	if cur.costMax < in.costMax {
		cur.costMax = in.costMax
	}

	if cur.costMin > in.costMin {
		cur.costMin = in.costMin
	}

	cur.costClosing = in.costClosing

	return cur
}

func transferMinutes(cur, to time.Time, minutes int) time.Time {
	var stime time.Time

	if to.Day() != cur.Day() {
		stime = time.Date(to.Year(), to.Month(), to.Day(), to.Hour(), to.Minute(), 0, 0, time.UTC)
	} else {
		diff := int(to.Sub(cur).Minutes()) - minutes
		stime = time.Date(to.Year(), to.Month(), to.Day(), to.Hour(), to.Minute()-diff, 0, 0, time.UTC)
	}

	return stime
}

func transferHours(cur, to time.Time, hours int) time.Time {
	var stime time.Time

	if to.Day() != cur.Day() {
		stime = time.Date(to.Year(), to.Month(), to.Day(), to.Hour(), 0, 0, 0, time.UTC)
	} else {
		stime = time.Date(to.Year(), to.Month(), to.Day(), cur.Hour()+hours, to.Minute(), 0, 0, time.UTC)
	}

	return stime
}

func createNewCandle(t trade) candle {
	var startTime time.Time
	quot := t.startTime.Minute() / 10          // nolint:gomnd // take the first digit of time
	diff := t.startTime.Minute() - 5 - 10*quot // nolint:gomnd // find position on 1...10

	if diff > 0 {
		startTime = time.Date(t.startTime.Year(), t.startTime.Month(), t.startTime.Day(), t.startTime.Hour(), 10*quot+t.startTime.Minute()-diff, 0, 0, time.UTC)
	} else {
		startTime = time.Date(t.startTime.Year(), t.startTime.Month(), t.startTime.Day(), t.startTime.Hour(), 10*quot+0, 0, 0, time.UTC)
	}

	c := candle{t.ticker, startTime, t.cost, t.cost, t.cost, t.cost, 5}

	return c
}

func getIndexByTicker(candles []candle, ticker string) int {
	index := -1

	for j, c := range candles {
		if c.ticker == ticker {
			index = j

			break
		}
	}

	return index
}

func candleClear(c candle) candle {
	c.costOpening = 0
	c.costMax = 0
	c.costMin = 0
	c.costClosing = 0

	return c
}

func difference(cur, t time.Time) int {
	return int(t.Sub(cur).Minutes())
}

func candle5m(in <-chan trade) (chan candle, chan candle) {
	out := make(chan candle)
	next := make(chan candle)
	cand := func() {
		var c5m []candle

		for t := range in {
			index := getIndexByTicker(c5m, t.ticker)

			if len(c5m) > 0 {
				if difference(c5m[0].startTime, t.startTime) >= c5m[0].minutes {
					for j, c := range c5m {
						stime := transferMinutes(c.startTime, t.startTime, c.minutes)

						if c.costOpening == 0 {
							c.startTime = stime
							c5m[j] = c

							continue
						}
						out <- c
						next <- c
						c = candleClear(c)
						c.startTime = stime
						c5m[j] = c
					}
				}
			}

			if index != -1 {
				c5m[index] = compare(c5m[index], t)
			} else {
				c := createNewCandle(t)
				c5m = append(c5m, c)
			}
		}

		for _, c := range c5m {
			if c.costOpening != 0 {
				out <- c
				next <- c
			}
		}

		close(out)
		close(next)
	}

	go cand()

	return out, next
}

func candle30m(in <-chan candle) (chan candle, chan candle) {
	out := make(chan candle)
	next := make(chan candle)
	cand := func() {
		var c30m []candle

		for t := range in {
			index := getIndexByTicker(c30m, t.ticker)

			if len(c30m) > 0 {
				if difference(c30m[0].startTime, t.startTime) >= c30m[0].minutes {
					for j, c := range c30m {
						stime := transferMinutes(c.startTime, t.startTime, c.minutes)

						if c.costOpening == 0 {
							c.startTime = stime
							c30m[j] = c

							continue
						}
						out <- c
						next <- c
						c = candleClear(c)
						c.startTime = stime
						c30m[j] = c
					}
				}
			}

			if index != -1 {
				c30m[index] = candleCompare(c30m[index], t)
			} else {
				c := candle{t.ticker, t.startTime, t.costOpening, t.costMax, t.costMin, t.costClosing, 30}
				c30m = append(c30m, c)
			}
		}

		for _, c := range c30m {
			if c.costOpening != 0 {
				out <- c
				next <- c
			}
		}

		close(out)
		close(next)
	}

	go cand()

	return out, next
}
func candle240m(in <-chan candle) chan candle {
	out := make(chan candle)
	cand := func() {
		var c240m []candle

		for t := range in {
			index := getIndexByTicker(c240m, t.ticker)

			if len(c240m) > 0 {
				if int(t.startTime.Sub(c240m[0].startTime).Hours()) >= (c240m[0].minutes / int(time.Hour/time.Minute)) {
					for j, c := range c240m {
						stime := transferHours(c.startTime, t.startTime, c240m[0].minutes/int(time.Hour/time.Minute))

						if c.costOpening == 0 {
							c.startTime = stime
							c240m[j] = c

							continue
						}
						out <- c
						c = candleClear(c)
						c.startTime = stime
						c240m[j] = c
					}
				}
			}

			if index != -1 {
				c240m[index] = candleCompare(c240m[index], t)
			} else {
				c := candle{t.ticker, t.startTime, t.costOpening, t.costMax, t.costMin, t.costClosing, 240}
				c240m = append(c240m, c)
			}
		}

		for _, c := range c240m {
			if c.costOpening != 0 {
				out <- c
			}
		}

		close(out)
	}

	go cand()

	return out
}

func openFile(filename string) *os.File {
	file, err := os.Create(filename)
	if err != nil {
		log.Fatal("cannot create the file: ", filename, ", err: ", err)
	}

	return file
}

func output(done chan interface{}, in ...<-chan candle) {
	var wg sync.WaitGroup

	file5m := openFile("candles_5m.csv")
	file30m := openFile("candles_30m.csv")
	file240m := openFile("candles_240m.csv")

	way := func(c <-chan candle) {
		defer wg.Done()

		var file *os.File

		var w *csv.Writer

		for value := range c {
			switch value.minutes {
			case 5: // nolint:gomnd // 5 minutes candle
				file = file5m
				w = csv.NewWriter(file5m)

			case 30: // nolint:gomnd // 30 minutes candle
				file = file30m
				w = csv.NewWriter(file30m)

			case 240: // nolint:gomnd // 240 minutes candle
				file = file240m
				w = csv.NewWriter(file240m)

			default:
				log.Fatal("bad value in switch-case!")
			}

			var str []string

			str = append(str, value.ticker, value.startTime.Format(time.RFC3339), strconv.FormatFloat(float64(value.costOpening), 'f', -1, 32),
				strconv.FormatFloat(float64(value.costMax), 'f', -1, 32), strconv.FormatFloat(float64(value.costMin), 'f', -1, 32),
				strconv.FormatFloat(float64(value.costClosing), 'f', -1, 32))

			err := w.Write(str)
			if err != nil {
				log.Fatal("can't write strings into file:", err)
			}

			w.Flush()
		}

		file.Close()
	}

	wg.Add(len(in))

	for _, i := range in {
		go way(i)
	}

	go func() {
		wg.Wait()
		done <- "end"
	}()
}

func main() {
	done := make(chan interface{})

	trade := reader()
	c5, toc30 := candle5m(trade)
	c30, toc240 := candle30m(toc30)
	c240 := candle240m(toc240)
	output(done, c5, c30, c240)

	for {
		select {
		case <-time.After(5 * time.Second): // nolint:gomnd // 5 seconds timeout
			log.Fatal("timeout")
		case <-done:
			fmt.Println("end!")
			return
		}
	}
}
